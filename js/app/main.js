// Clear forms
(function ($) {
	'use strict';

	$.fn.clearDefault = function () {
		return this.each(function () {
			var defaultValue = $(this).val();
			$(this).focus(function () {
				if ($(this).val() === defaultValue) {
					$(this).val('');
				}
			});
			$(this).blur(function () {
				if ($(this).val() === '') {
					$(this).val(defaultValue);
				}
			});
		});
	};
})(jQuery);

(function () {
	'use strict';

	$('.clear-text').clearDefault();

	$(".menu .button-mobile").click(function (e) {
		e.preventDefault();
		$(this).toggleClass("open");
		$(".menu ul").toggleClass("open");
	});

	$('.item-title').click(function (j) {

		var dropDown = $(this).closest('.item').find('.item-content');
		$(this).closest('.item').find('.item-content').not(dropDown).slideUp();

		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			$(this).closest('.item').find('.item-title.active').removeClass('active');
			$(this).addClass('active');
		}

		dropDown.stop(false, true).slideToggle();
		j.preventDefault();
	});

})();