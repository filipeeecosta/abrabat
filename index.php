<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie8 oldie" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="pt-br"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Abrabat</title>
  <meta name="author"      content="Abrabat">
  <meta name="description" content="">
  <meta name="keywords"    content="">
  <meta name="viewport"    content="width=device-width, initial-scale=1">
  <!-- twitter card -->
  <meta name="twitter:card"        content="summary">
  <meta name="twitter:image"       content="">
  <meta name="twitter:title"       content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:creator"     content="">
  <!-- open graph -->
  <meta property="og:locale"      content="pt_BR">
  <meta property="og:type"        content="website">
  <meta property="og:title"       content="">
  <meta property="og:description" content="">
  <meta property="og:url"         content="">
  <meta property="og:image"       content="">
  <meta property="og:site_name"   content="">
  <!-- add to homescreen for chrome on android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="img/content/chrome-touch-icon-192x192.png">
  <!-- add to homescreen for safari on ios -->
  <meta name="apple-mobile-web-app-capable"          content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title"            content=" ">
  <link rel="apple-touch-icon-precomposed" href="img/content/apple-touch-icon-precomposed.png">
  <!-- tile icon for win8 -->
  <meta name="msapplication-TileImage" content="img/content/ms-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#81cfff">
  <!-- favicon -->
  <link rel="shortcut icon" href="img/content/favicon.ico">
  <link rel="icon"          href="img/content/favicon.ico">
  <!-- styles -->
  <!-- SEO tag href="url atual" / hreflang="" -->
  <link rel="alternate"  href="" hreflang="pt">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <style type="text/css"><?php echo file_get_contents('css/style.css') ?></style>
</head>
<body>
  <nav class="screen-reader">
    <a href="#content" accesskey="c">Alt + Shift + C ir para o conteúdo</a>
    <a href="#nav" accesskey="m">Alt + Shift + M ir para o menu</a>
    <a href="#search" accesskey="b">Alt + Shift + B ir para a busca</a>
    <a href="#footer" accesskey="f">Alt + Shift + F ir para o rodapé</a>
  </nav><!-- .screen-reader -->
  
  <div class="wrap">
    <header class="header">
      <div class="border"><div class="inner"></div></div>

      <a href="#" class="brand"><img src="img/layout/logo_abrabat.png" alt="Abrabat" /></a>
      
      <nav class="menu">
        <a href="" class="button-mobile">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </a><!-- btn mobile -->
        
        <ul>
          <li><a href="">Página Inicial</a></li>
          <li><a href="">Notícias</a></li>
          <li><a href="">Perguntas Frequentes</a></li>
          <li><a href="">Contato</a></li>
        </ul>
      </nav>

      <div class="title-page">
        <i class="icon icon-battery"></i>
        <h1>VERIFIQUE SE A BATERIA<br />ESTÁ APROVADA</h1>
        <p>Para verificar com facilidade e segurança que uma bateria possui certificação<br />IMMETRO, siga os passos abaixo:</p>
      </div>
    </header><!-- .header -->
    
    <section class="content">

      <div class="container">
        <div class="latest-news">

          <h2 class="section-title">Últimas Notícias</h2>

          <div class="news">

            <div class="featured-news inner-caption">
              <a href="#">
                <img src="https://via.placeholder.com/570x555" alt="">
                <div class="caption">
                  <span class="date"><b>24</b>Junho, 2019</span>
                  <p>Paullum deliquit, the ponderibus mode se lisque suis ratio</p>
                </div>
              </a>
            </div><!-- featured news -->

            <div class="list-news">
              <article>
                <a href="">
                  <img src="https://via.placeholder.com/270x170" alt="">
                  <div class="content-news">
                    <span class="date"><b>24</b>Junho, 2019</span>
                    <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                    <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  </div>
                </a>
              </article><!-- post news -->

              <article>
                <a href="">
                  <img src="https://via.placeholder.com/270x170" alt="">
                  <div class="content-news">
                    <span class="date"><b>24</b>Junho, 2019</span>
                    <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                    <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  </div>
                </a>
              </article><!-- post news -->

              <article>
                <a href="">
                  <img src="https://via.placeholder.com/270x170" alt="">
                  <div class="content-news">
                    <span class="date"><b>24</b>Junho, 2019</span>
                    <h3>Nec dubitamus multa iter quae et nos invenerat</h3>
                    <p>Tityre tur patulae recubans sub tegmine fagi  dolorue that entar quoque …</p>
                  </div>
                </a>
              </article><!-- post news -->
              

            </div>
          </div>
          
        </div><!-- latest news -->
      </div>

      <div class="more-separator">
        <a href="" class="btn btn-default">Mais notícias</a>
      </div><!-- separator -->

      <div class="container">
        <div class="steps">
          <h3>5 Etapas Fáceis Para Verificar Seu Registro</h3>
          <ul>
            <li>
              <img src="img/layout/passo_01.png" alt="" />
              <p>Identifique o número de registro que está dentro do selo do INMETRO, e o modelo da bateria, ao lado dos dados técnicos, no canto inferior esquerdo.</p>
            </li>

            <li>
              <img src="img/layout/passo_02.png" alt="" />
              <p>Acesse o site do INMETRO usando o botão acima e preencha o número de registro no respectivo campo e clique em Pesquisar</p>
            </li>

            <li>
              <img src="img/layout/passo_03.png" alt="" />
              <p>Se o número de registro for válido, as informações do fabricante aparecerão. Em seguida, clique no botão "ver detalhes"</p>
            </li>

            <li>
              <img src="img/layout/passo_04.png" alt="" />
              <p>Mais abaixo na página, uma lista de todas as baterias certificadas do fabricante aparecerá. Se você não consegue encontrá-lo, você pode filtrá-lo colocando o modelo da bateria neste campo.</p>
            </li>

            <li>
              <img src="img/layout/passo_05.png" alt="" />
              <p>Se o produto for certificado, seus dados aparecerão na lista. Mas se você não tiver certificação, nenhum dado será retornado.</p>
            </li>
          </ul>

          <a href="#" class="btn btn-green">Verifique Sua Bateria Agora!</a>
        </div><!-- steps -->
      </div>

      <div class="abrabat slope left">
        <div class="border"><div class="inner"></div></div>

        <img src="img/layout/logo_abrabat_oval.png" alt="" />

        <p>A <b>ABRABAT</b> é composta pelos principais fabricantes nacionais, <br />bem como de parceiros no processo de fabricação.</p>
      </div><!-- abrabat -->

      <div class="container">
        <div class="certified-inmetro">
          <div class="description">
            <i class="icon icon_certified"></i>
            <p>A certificação compulsória das baterias trouxe vários benefícios para o consumidor final. Porém, não é garantia de que todas as baterias tenham o mesmo nível de qualidade, conforme mostra o quadro abaixo: </p>
          </div>

          <div class="comparative">
            <div class="ensure ensured">
              <h4 class="title-ensure">
                <i class="icon icon_title"></i>
                O QUE A CERTIFICAÇÃO <b>GARANTE</b>
              </h4>

              <ul class="list-ensure">
                <li>
                  <i class="icon icon_checked"></i>
                  <p>Que os processos de fabricação estejam adequados aos padrões estabelecidos pelo INMETRO, bem como que os resíduos gerados durante a fabricação tenham destinação adequada;</p>
                </li>

                <li>
                  <i class="icon icon_checked"></i>
                  <p>Informação sobre a origem do produto, onde é fabricado, a razão social da empresa, data de fabricação e um telefone para atendimento ao consumidor (SAC);</p>
                </li>

                <li>
                  <i class="icon icon_checked"></i>
                  <p>Que o produto atenda as capacidades elétricas descritas no rótulo, como Capacidade Nominal (Ah), Reserva de Capacidade (RC) e Partida a Frio (CCA) conforme norma ABNT;</p>
                </li>

                <li>
                  <i class="icon icon_checked"></i>
                  <p>O tipo de tecnologia aplicada na fabricação da bateria o que garante a sua classificação como Livre de Manutenção, Baixa Manutenção ou Com Manutenção e também se a bateria é do tipo Regulada por Válvula;</p>
                </li>
              </ul>
            </div>

            <div class="ensure not-ensured">
              <h4 class="title-ensure">
                <i class="icon icon_title"></i>
                O QUE A CERTIFICAÇÃO <b>NÃO GARANTE</b>
              </h4>

              <ul class="list-ensure">
                <li>
                  <i class="icon icon_close"></i>
                  <p>A durabilidade do produto, pois este não é um item verificado durante o processo de certificação;</p>
                </li>

                <li>
                  <i class="icon icon_close"></i>
                  <p>Que o produto está livre de qualquer defeito de fabricação;</p>
                </li>

                <li>
                  <i class="icon icon_close"></i>
                  <p>Que a matéria-prima utilizada tenha um alto grau de pureza, o que seria necessário para garantir o bom funcionamento da bateria durante toda sua vida útil.</p>
                </li>
              </ul>
            </div>
          </div>

          <div class="description-inmetro">
            <div class="box-icon"><i class="icon icon_inmetro"></i></div>
            <p>O fabricante que não atender algum dos critérios estabelecidos pela regulamentação do INMETRO terá como pena a suspensão do registro da família do produto em questão, o que impede a sua comercialização, conforme descrito na Portaria 301 – Requisitos de Avaliação da Conformidade para Componentes Automotivos – Item 6.2.3.</p>
          </div>
        </div>
      </div><!-- certified -->

      <div class="book-abrabat slope right">
        <div class="border"><div class="inner"></div></div>

        <div class="description">
          <img src="img/layout/bandeira_inmetro.png" alt="" />

          <div class="description-content">
            <h4>Livreto do Inmetro</h4>
            <p>Ut enim ad minim veniam, quis nostrud exercitation. Petierunt uti sibi concilium totius.</p>
            <a href="" class="btn btn-transparent">Baixar</a>
          </div>
        </div>
      </div><!-- book inmetro -->

      <div class="about">
        <div class="container">
          <div class="description">
            <i class="icon icon_users"></i>
            <h2 class="section-title">Quem Nós Somos?</h2>
            <p>A <span>Assossiação Brasileira de Baterias Automotivas e Industriais</span><br /> é uma organização sem fins lucrativos.</p>

            <b><span>Nossos</span> Principais Objetivos <span>São:</span></b>
          </div>

          <div class="about-list">
            <ul>
              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Promover o comércio de baterias automotivas e industriais</strong> observando os mais rigorosos padrões de qualidade e o respeito ao consumidor.</p>
              </li>

              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Promover ações que apóiem a manutenção do meio ambiente</strong> através de processos de fabricação de acordo com as licenças ambientais concedidas pelos órgãos competentes e a reciclagem correspondente aos produtos vendidos ao mercado de reposição.</p>
              </li>

              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Promover um ambiente fiscal e tributário</strong> que permita um marketing saudável, reduzindo o custo do nosso produto para os consumidores.</p>
              </li>

              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Promover o desenvolvimento e treinamento</strong> de mão de obra qualificada.</p>
              </li>
            </ul>

            <ul>
              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Apoiar o processo de certificação de produtos</strong> através de ações que permitam às autoridades competentes distinguir facilmente produtos certificados ou não certificados nos vários níveis da cadeia de distribuição no mercado.</p>
              </li>

              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Promover o desenvolvimento tecnológico do setor.</strong></p>
              </li>

              <li>
                <i class="icon icon_checked"></i>
                <p><strong>Defender os interesses de seus associados</strong> com o objetivo de coibir quaisquer ilegalidades que afetem os direitos dos mesmos e do setor de baterias automotivas e industriais em geral.</p>
              </li>
            </ul>

          </div>
        </div>
      </div>
      
    </section><!-- .content -->

    <footer class="footer">
      <div class="border"><div class="inner"></div></div>

      <img src="img/layout/logo_abrabat_oval.png" alt="" class="logo-footer" />

      <ul class="menu-footer">
        <li><a href="">Produtos Certificados</a></li>
        <li><a href="">Obrigações do Comerciante</a></li>
        <li><a href="">Obrigações do Fabricante</a></li>
        <li><a href="">Direitos do Consumidor</a></li>
      </ul>
    </footer><!-- .footer -->
  </div><!-- .wrap -->

  <!-- scripts -->
  <script src="js/lib/modernizr.js"></script>
  <script src="js/lib/jquery-3.2.1.min.js"></script>
  <script src="js/app/main.js"></script>
</body>
</html>
