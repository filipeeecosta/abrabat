<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie8 oldie" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="pt-br"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Abrabat</title>
  <meta name="author"      content="Abrabat">
  <meta name="description" content="">
  <meta name="keywords"    content="">
  <meta name="viewport"    content="width=device-width, initial-scale=1">
  <!-- twitter card -->
  <meta name="twitter:card"        content="summary">
  <meta name="twitter:image"       content="">
  <meta name="twitter:title"       content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:creator"     content="">
  <!-- open graph -->
  <meta property="og:locale"      content="pt_BR">
  <meta property="og:type"        content="website">
  <meta property="og:title"       content="">
  <meta property="og:description" content="">
  <meta property="og:url"         content="">
  <meta property="og:image"       content="">
  <meta property="og:site_name"   content="">
  <!-- add to homescreen for chrome on android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="img/content/chrome-touch-icon-192x192.png">
  <!-- add to homescreen for safari on ios -->
  <meta name="apple-mobile-web-app-capable"          content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title"            content=" ">
  <link rel="apple-touch-icon-precomposed" href="img/content/apple-touch-icon-precomposed.png">
  <!-- tile icon for win8 -->
  <meta name="msapplication-TileImage" content="img/content/ms-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#81cfff">
  <!-- favicon -->
  <link rel="shortcut icon" href="img/content/favicon.ico">
  <link rel="icon"          href="img/content/favicon.ico">
  <!-- styles -->
  <!-- SEO tag href="url atual" / hreflang="" -->
  <link rel="alternate"  href="" hreflang="pt">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <style type="text/css"><?php echo file_get_contents('css/style.css') ?></style>
</head>
<body>
  <nav class="screen-reader">
    <a href="#content" accesskey="c">Alt + Shift + C ir para o conteúdo</a>
    <a href="#nav" accesskey="m">Alt + Shift + M ir para o menu</a>
    <a href="#search" accesskey="b">Alt + Shift + B ir para a busca</a>
    <a href="#footer" accesskey="f">Alt + Shift + F ir para o rodapé</a>
  </nav><!-- .screen-reader -->
  
  <div class="wrap">
    <header class="header internal">
      <div class="border"><div class="inner"></div></div>

      <a href="#" class="brand"><img src="img/layout/logo_abrabat.png" alt="Abrabat" /></a>
      
      <nav class="menu">
        <a href="" class="button-mobile">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </a><!-- btn mobile -->
        
        <ul>
          <li><a href="">Página Inicial</a></li>
          <li><a href="">Notícias</a></li>
          <li><a href="">Perguntas Frequentes</a></li>
          <li><a href="">Contato</a></li>
        </ul>
      </nav>

      <div class="title-page">
        <h1>Contato</h1>
      </div>
    </header><!-- .header -->
    
    <section class="content">

    <div class="container">
        <div class="page-contact">
            <h2 class="section-title">Nós Amamos Ouvir Você!</h2>
            <h3 class="subtitle">Vivamus sagittis lacus vel augue laoreet rutrum faucibus.</h3>    

            <div class="contact-info">
                <section>
                    <i class="icon icon-message"></i>
                    <p>E-mail</p>
                    <a href="">contato@abrabat.com.br</a>
                </section>
                <section>
                    <i class="icon icon-globe"></i>
                    <p>Endereço</p>
                    <span>Me non paenitet nullum festiviorem<br />excogitasse ad hoc.</span>
                </section>
            </div>

            <div class="box-form">
                <form action="form">
                    <h3 class="title-form">Contato</h3>
                    
                    <input type="text" class="form-control" placeholder="Nome*">
                    <input type="text" class="form-control col" placeholder="E-mail*">
                    <input type="text" class="form-control col" placeholder="Telefone*">

                    <textarea name="" id="" cols="30" rows="10" class="form-control" placeholder="Mensagem"></textarea>

                    <button class="btn btn-green">Enviar mensagem</button>
                </form>
            </div>
        </div>

        <div class="steps">
          <h3>5 Etapas Fáceis Para Verificar Seu Registro</h3>
          <ul>
            <li>
              <img src="img/layout/passo_01.png" alt="" />
              <p>Identifique o número de registro que está dentro do selo do INMETRO, e o modelo da bateria, ao lado dos dados técnicos, no canto inferior esquerdo.</p>
            </li>

            <li>
              <img src="img/layout/passo_02.png" alt="" />
              <p>Acesse o site do INMETRO usando o botão acima e preencha o número de registro no respectivo campo e clique em Pesquisar</p>
            </li>

            <li>
              <img src="img/layout/passo_03.png" alt="" />
              <p>Se o número de registro for válido, as informações do fabricante aparecerão. Em seguida, clique no botão "ver detalhes"</p>
            </li>

            <li>
              <img src="img/layout/passo_04.png" alt="" />
              <p>Mais abaixo na página, uma lista de todas as baterias certificadas do fabricante aparecerá. Se você não consegue encontrá-lo, você pode filtrá-lo colocando o modelo da bateria neste campo.</p>
            </li>

            <li>
              <img src="img/layout/passo_05.png" alt="" />
              <p>Se o produto for certificado, seus dados aparecerão na lista. Mas se você não tiver certificação, nenhum dado será retornado.</p>
            </li>
          </ul>

          <a href="#" class="btn btn-green">Verifique Sua Bateria Agora!</a>
        </div><!-- steps -->
      </div>
      
    </section><!-- .content -->

    <footer class="footer">
      <div class="border"><div class="inner"></div></div>

      <img src="img/layout/logo_abrabat_oval.png" alt="" class="logo-footer" />

      <ul class="menu-footer">
        <li><a href="">Produtos Certificados</a></li>
        <li><a href="">Obrigações do Comerciante</a></li>
        <li><a href="">Obrigações do Fabricante</a></li>
        <li><a href="">Direitos do Consumidor</a></li>
      </ul>
    </footer><!-- .footer -->
  </div><!-- .wrap -->

  <!-- scripts -->
  <script src="js/lib/modernizr.js"></script>
  <script src="js/lib/jquery-3.2.1.min.js"></script>
  <script src="js/app/main.js"></script>
</body>
</html>
