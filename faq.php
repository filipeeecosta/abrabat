<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie8 oldie" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="pt-br"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Abrabat</title>
  <meta name="author"      content="Abrabat">
  <meta name="description" content="">
  <meta name="keywords"    content="">
  <meta name="viewport"    content="width=device-width, initial-scale=1">
  <!-- twitter card -->
  <meta name="twitter:card"        content="summary">
  <meta name="twitter:image"       content="">
  <meta name="twitter:title"       content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:creator"     content="">
  <!-- open graph -->
  <meta property="og:locale"      content="pt_BR">
  <meta property="og:type"        content="website">
  <meta property="og:title"       content="">
  <meta property="og:description" content="">
  <meta property="og:url"         content="">
  <meta property="og:image"       content="">
  <meta property="og:site_name"   content="">
  <!-- add to homescreen for chrome on android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="img/content/chrome-touch-icon-192x192.png">
  <!-- add to homescreen for safari on ios -->
  <meta name="apple-mobile-web-app-capable"          content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title"            content=" ">
  <link rel="apple-touch-icon-precomposed" href="img/content/apple-touch-icon-precomposed.png">
  <!-- tile icon for win8 -->
  <meta name="msapplication-TileImage" content="img/content/ms-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#81cfff">
  <!-- favicon -->
  <link rel="shortcut icon" href="img/content/favicon.ico">
  <link rel="icon"          href="img/content/favicon.ico">
  <!-- styles -->
  <!-- SEO tag href="url atual" / hreflang="" -->
  <link rel="alternate"  href="" hreflang="pt">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <style type="text/css"><?php echo file_get_contents('css/style.css') ?></style>
</head>
<body>
  <nav class="screen-reader">
    <a href="#content" accesskey="c">Alt + Shift + C ir para o conteúdo</a>
    <a href="#nav" accesskey="m">Alt + Shift + M ir para o menu</a>
    <a href="#search" accesskey="b">Alt + Shift + B ir para a busca</a>
    <a href="#footer" accesskey="f">Alt + Shift + F ir para o rodapé</a>
  </nav><!-- .screen-reader -->
  
  <div class="wrap">
    <header class="header internal">
      <div class="border"><div class="inner"></div></div>

      <a href="#" class="brand"><img src="img/layout/logo_abrabat.png" alt="Abrabat" /></a>
      
      <nav class="menu">
        <a href="" class="button-mobile">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </a><!-- btn mobile -->
        
        <ul>
          <li><a href="">Página Inicial</a></li>
          <li><a href="">Notícias</a></li>
          <li><a href="">Perguntas Frequentes</a></li>
          <li><a href="">Contato</a></li>
        </ul>
      </nav>

      <div class="title-page">
        <h1>Perguntas Frequentes</h1>
      </div>
    </header><!-- .header -->
    
    <section class="content">

      <div class="container">
        <div class="faq-group">
            <h2 class="section-title">Obrigação dos Fabricantes</h2>

            <div class="item-group">
                <p class="legend">Clique nos itens para visualizar os detalhes</p>
                
                <div class="item">
                    <div class="item-title">
                        <b class="item-number">1</b>
                        <p>Vender baterias somente com certificação do Inmetro</p>
                        <span class="item-arrow"></span>
                    </div>
                    <div class="item-content">
                        <p>A venda de baterias sem a certificação do Inmetro é considerada crime contra as relações de consumo. Esse crime está previsto no art. 7º, incisos II e IX, da Lei nº 8.137, de 27 de dezembro de 1990. A pena prevista é de detenção, de 02 a 05 anos, ou multa. <br />Link: <a href="http://www.planalto.gov.br/ccivil_03/leis/l8137.html" target="_blank">http://www.planalto.gov.br/ccivil_03/leis/l8137.html</a></p>
                    </div>
                </div>

                <div class="item">
                    <div class="item-title">
                        <b class="item-number">2</b>
                        <p>Vender baterias somente com certificação do Inmetro</p>
                        <span class="item-arrow"></span>
                    </div>
                    <div class="item-content">
                        <p>A venda de baterias sem a certificação do Inmetro é considerada crime contra as relações de consumo. Esse crime está previsto no art. 7º, incisos II e IX, da Lei nº 8.137, de 27 de dezembro de 1990. A pena prevista é de detenção, de 02 a 05 anos, ou multa. <br />Link: <a href="http://www.planalto.gov.br/ccivil_03/leis/l8137.html" target="_blank">http://www.planalto.gov.br/ccivil_03/leis/l8137.html</a></p>
                    </div>
                </div>

                <div class="item">
                    <div class="item-title">
                        <b class="item-number">3</b>
                        <p>Vender baterias somente com certificação do Inmetro</p>
                        <span class="item-arrow"></span>
                    </div>
                    <div class="item-content">
                        <p>A venda de baterias sem a certificação do Inmetro é considerada crime contra as relações de consumo. Esse crime está previsto no art. 7º, incisos II e IX, da Lei nº 8.137, de 27 de dezembro de 1990. A pena prevista é de detenção, de 02 a 05 anos, ou multa. <br />Link: <a href="http://www.planalto.gov.br/ccivil_03/leis/l8137.html" target="_blank">http://www.planalto.gov.br/ccivil_03/leis/l8137.html</a></p>
                    </div>
                </div>
            </div>
            
        </div>
      </div>
      
    </section><!-- .content -->

    <footer class="footer">
      <div class="border"><div class="inner"></div></div>

      <img src="img/layout/logo_abrabat_oval.png" alt="" class="logo-footer" />

      <ul class="menu-footer">
        <li><a href="">Produtos Certificados</a></li>
        <li><a href="">Obrigações do Comerciante</a></li>
        <li><a href="">Obrigações do Fabricante</a></li>
        <li><a href="">Direitos do Consumidor</a></li>
      </ul>
    </footer><!-- .footer -->
  </div><!-- .wrap -->

  <!-- scripts -->
  <script src="js/lib/modernizr.js"></script>
  <script src="js/lib/jquery-3.2.1.min.js"></script>
  <script src="js/app/main.js"></script>
</body>
</html>
