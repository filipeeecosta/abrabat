<!DOCTYPE html>
<!--[if IE 8]><html class="no-js ie8 oldie" lang="pt-br"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="pt-br"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Bresa</title>
  <meta name="author"      content="Bresa">
  <meta name="description" content="">
  <meta name="keywords"    content="">
  <meta name="viewport"    content="width=device-width, initial-scale=1">
  <!-- twitter card -->
  <meta name="twitter:card"        content="summary">
  <meta name="twitter:image"       content="">
  <meta name="twitter:title"       content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:creator"     content="">
  <!-- open graph -->
  <meta property="og:locale"      content="pt_BR">
  <meta property="og:type"        content="website">
  <meta property="og:title"       content="">
  <meta property="og:description" content="">
  <meta property="og:url"         content="">
  <meta property="og:image"       content="">
  <meta property="og:site_name"   content="">
  <!-- add to homescreen for chrome on android -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="icon" sizes="192x192" href="img/content/chrome-touch-icon-192x192.png">
  <!-- add to homescreen for safari on ios -->
  <meta name="apple-mobile-web-app-capable"          content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title"            content=" ">
  <link rel="apple-touch-icon-precomposed" href="img/content/apple-touch-icon-precomposed.png">
  <!-- tile icon for win8 -->
  <meta name="msapplication-TileImage" content="img/content/ms-touch-icon-144x144-precomposed.png">
  <meta name="msapplication-TileColor" content="#81cfff">
  <!-- favicon -->
  <link rel="shortcut icon" href="img/content/favicon.ico">
  <link rel="icon"          href="img/content/favicon.ico">
  <!-- styles -->
  <!-- SEO tag href="url atual" / hreflang="" -->
  <link rel="alternate"  href="" hreflang="pt">
  <style type="text/css"><?php echo file_get_contents('css/style.css') ?></style>
</head>
<body>
  <nav class="screen-reader">
    <a href="#content" accesskey="c">Alt + Shift + C ir para o conteúdo</a>
    <a href="#nav" accesskey="m">Alt + Shift + M ir para o menu</a>
    <a href="#search" accesskey="b">Alt + Shift + B ir para a busca</a>
    <a href="#footer" accesskey="f">Alt + Shift + F ir para o rodapé</a>
  </nav><!-- .screen-reader -->
  <div class="container">
    <header class="header">
      <h1>Bresa</h1>
    </header><!-- #header -->
    <section class="content">
      
      <div class="container">
        <form action="#" id="form" enctype="multipart/form-data" class="form">
          <label class="form-field">
            <span class="form-label">Nome</span>
            <input type="text" name="name" id="name" class="form-text required">
          </label>
          <label class="form-field">
            <span class="form-label">E-mail</span>
            <input type="text" name="email" id="email" class="form-text required">
          </label>
          <label class="form-field">
            <span class="form-label">Celular</span>
            <input type="text" name="celular" id="telefone-celular" class="form-text mask-phone">
          </label>
          <div class="form-field form-field-select">
            <span class="form-label">Área de Atuação</span>
            <span class="form-text"></span>
            <select name="atuacao">
              <option value="Iten 1">Iten 1</option>
              <option value="Iten 2">Iten 2</option>
              <option value="Iten 3">Iten 3</option>
              <option value="Iten 4">Iten 4</option>
            </select>
          </div>
          <label class="form-field form-field-file form-focus">
            <span class="form-label">Arquivo</span>
            <input type="file" name="file" id="file" class="file">
            <p class="form-text"></p>
            <div class="form-text-btn">Anexar</div>
          </label>
          <div class="form-msg">
            <p class="msg-error">Mensagem não enviada. Verifique o preenchimento do campo e tente novamente.</p>
            <p class="msg-success">Mensagem enviada com sucesso!</p>
          </div>
          <input type="submit" value="Enviar" class="form-submit" data-text="Enviar">
        </form>
      </div>
      
    </section><!-- #content -->
    <footer class="footer">
      <div class="icon"></div>
      <section class="by"><a href="http://www.fishy.com.br" target="_blank" title="Fishy">Made with ♥ by Fishy</a></section>
    </footer><!-- #footer -->
  </div><!-- #container -->
  <!-- scripts -->
  <script src="js/lib/modernizr.js"></script>
  <script src="js/lib/jquery-3.2.1.min.js"></script>
  <script src="js/lib/owl.carousel.min.js"></script>
  <script src="js/lib/wow.min.js"></script>
  <script src="js/app/form.js"></script>
  <script src="js/app/main.js"></script>
</body>
</html>
